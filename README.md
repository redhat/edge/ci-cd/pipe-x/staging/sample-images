# Automotive sample images

This is the git repository for the Automotive SIG sample images. It contains recipes for
building some sample automotive images and information on how to download them.

For more information, visit:

* [Automotive SIG wiki page] (https://wiki.centos.org/SpecialInterestGroup/Automotive)
* [Automotive SIG documentation] (https://sigs.centos.org/automotive)

This is a test MR.
